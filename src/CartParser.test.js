import CartParser from './CartParser';
import {readFileSync} from "fs";
const path = require('path');
const csvFilePath = path.join(__dirname, '../samples/cart.csv');
const cartFilePath = path.join(__dirname, '../samples/cart.json')
let parser, validate, contents, parse, parseLine, createError, calcTotal;

beforeEach(() => {
	parser = new CartParser();
	contents = parser.readFile.bind(parser)(csvFilePath);
	validate = parser.validate.bind(parser);
	parse = parser.parse.bind(parser);
	parseLine = parser.parseLine.bind(parser);
	createError = parser.createError.bind(parser);
	calcTotal = parser.calcTotal.bind(parser);
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.
	it("should give a validation error for incorrect headers in the file", () => {

		const res = validate(' name,Price!,Quanity\n')

		expect(res).toHaveLength(3);
		expect(res[0].message).toBe('Expected header to be named "Product name" but received name.');
		expect(res[1].message).toBe('Expected header to be named "Price" but received Price!.');
		expect(res[2].message).toBe('Expected header to be named "Quantity" but received Quanity.');
	});

	it("should throw a validation error on incorrect amount of input", () => {

		const res1 = validate('Product name,Price,Quantity\nMollis consequat9.002\nTvoluptatem,10.32,1\n')
		const res2 = validate('Product name,Price,Quantity\nMollis consequat9.00,2\nTvoluptatem,10.32,1\n')

		expect(res1[0].message).toBe('Expected row to have 3 cells but received 1.');
		expect(res2[0].message).toBe('Expected row to have 3 cells but received 2.');
	});

	it("should throw a validation error on the wrong input type", () => {

		const res = validate('Product name,Price,Quantity\n,9.00,aaa\nTvoluptatem,Price,1\n')

		expect(res[0].message).toBe('Expected cell to be a nonempty string but received "".');
		expect(res[1].message).toBe('Expected cell to be a positive number but received "aaa".');
		expect(res[2].message).toBe('Expected cell to be a positive number but received "Price".');
	});

	it("should give an empty array when correct data", () => {
		const res = validate(contents);
		expect(res).toHaveLength(0);
	});

	it("should throw a validation error if the validation is unsuccessful", () => {
		expect(()=> {
			parse(cartFilePath)
		}).toThrow('Validation failed!')
	});

	it("should return object with keys: items and total when no error", () => {

		const res = parse(csvFilePath);

		expect(Object.keys(res)).toEqual(['items','total'])
	});

	it("should return object with keys: name, price, quantity and id", () => {

		const res = parseLine('Mollis consequat,9.00,2');

		expect(Object.keys(res)).toEqual(['name', 'price', 'quantity', 'id'])
	});

	it("should return object with keys: type, row, column and message when no error", () => {

		const res = createError('header', 2, 3, 'Error');

		expect(Object.keys(res)).toEqual(['type', 'row', 'column', 'message'])
	});

	it("should return object with keys: type, row, column and message when no error", () => {

		const data = [{price: 12, quantity: 1}, {price: 10.5, quantity: 2}]

		const res = calcTotal(data);

		expect(res).toBe(33)
	});

});

describe('CartParser - integration test', () => {
	// Add your integration test here.

	it("should return an object as in the example when there are no errors", () => {

		const successResult = JSON.parse(readFileSync(cartFilePath, 'utf-8', 'r'));
		const res = parse(csvFilePath);

		for (let i = 0; i < res.items.length; i++) {
			delete res.items[i].id
			delete successResult.items[i].id
		}

		expect(res.items).toHaveLength(successResult.items.length);
		expect(res.total).toBe(successResult.total);
		expect(res).toEqual(successResult)
	});

});
